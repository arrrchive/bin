worker_processes 1;
 
events { worker_connections 1024; }
 
http {
 
    sendfile on;

    upstream demo-trile-link-reverse-proxy-upstream {
        server trile-demo-federation-controller-nginx:443;
    }

    upstream landing-page-upstream {
        server trile-landing-page:3000;
    }

    server {
        listen 80;
        server_name trile.link www.trile.link;
        server_tokens off;

        location / {
            return 301 https://$host$request_uri;
        }
 
    }

    server {
        listen 443 ssl;
        server_name www.trile.link trile.link;
        server_tokens off;

        ssl_certificate /etc/letsencrypt/live/demo.trile.link/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/demo.trile.link/privkey.pem;
        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location / {
            proxy_pass         http://landing-page-upstream;
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
        }
    }

    server {
        listen 80;
        server_name demo.trile.link *.demo.trile.link;
        server_tokens off;

        location / {
            proxy_pass         http://demo-trile-link-reverse-proxy-upstream;
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
        }
 
    }

    server {
        listen 443;
        server_name demo.trile.link *.demo.trile.link;
        server_tokens off;

        ssl_certificate /etc/letsencrypt/live/demo.trile.link/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/demo.trile.link/privkey.pem;
        include /etc/letsencrypt/options-ssl-nginx.conf;
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

        location / {
            proxy_pass         https://demo-trile-link-reverse-proxy-upstream;
            proxy_redirect     off;
            proxy_set_header   Host $host;
            proxy_set_header   X-Real-IP $remote_addr;
            proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header   X-Forwarded-Host $server_name;
        }
    }
}
